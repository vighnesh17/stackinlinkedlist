public class stackLinkedList {
    class Node{
        int data;
        Node next;
        Node(int val){
            data=val;
            next=null;
        }
    }
    Node top;
    stackLinkedList(){
        top= null;
    }
    public void push(int val){

        Node newnode=new Node(val);
        newnode.next=top;
        top=newnode;
    }
    public int pop(){
        if(top==null)
            throw new IndexOutOfBoundsException("list is empty");
        int temp= top.data;
        top=top.next;
        return temp;
    }
    public int peek(){
        return  top.data;
    }
}
